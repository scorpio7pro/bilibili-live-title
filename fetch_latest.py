# -*- coding: UTF-8 -*-
__author__ = 'Charles'
import os
import requests
from bs4 import BeautifulSoup
if __name__ == '__main__':
    urlprefix = url = 'http://localhost/~charles/{0}/title.html'.format
    urls = ['213', '528', '692']
    for i in urls:
        r = requests.get(urlprefix(i))
        r.encoding = 'utf-8'
        b = BeautifulSoup(r.text, features="lxml")
        titles = b.findAll('li')
        with open(os.path.join(i, 'README.md'), 'w') as f:
            f.write('# {0}直播间标题记录\n\n'.format(i))
            for j in titles:
                if len(j.contents) == 1:
                    f.write('- {0}\n'.format(j.contents[0]))
